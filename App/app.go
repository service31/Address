package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"net/http"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	uuid "github.com/satori/go.uuid"
	service "gitlab.com/service31/Address/Service"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	entity "gitlab.com/service31/Data/Entity/Address"
	module "gitlab.com/service31/Data/Module/Address"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	echoEndpoint  = flag.String("endpoint", "localhost:8000", "endpoint")
	serverChannel chan int
)

type server struct {
	module.UnimplementedAddressServiceServer
}

func (s *server) Create(ctx context.Context, in *module.CreateAddressRequest) (*module.AddressDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	response, err := service.CreateAddress(entityID, in)
	if err != nil {
		return nil, err
	}
	return response.ToDTO(), nil
}

func (s *server) GetAddressByID(ctx context.Context, in *module.GetAddressByIDRequest) (*module.AddressDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	response, err := service.GetAddressByID(ID)
	if err != nil {
		return nil, err
	}
	return response.ToDTO(), nil
}

func (s *server) GetDefaultAddressByEntity(ctx context.Context, in *module.GetDefaultAddressByEntityRequest) (*module.AddressDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	response, err := service.GetDefaultAddressByEntity(entityID)
	if err != nil {
		return nil, err
	}
	return response.ToDTO(), nil
}

func (s *server) GetAddressesByEntityID(ctx context.Context, in *module.GetAddressesByEntityIDRequest) (*module.RepeatedAddressDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	response, err := service.GetAddressesByEntityID(entityID)
	if err != nil {
		return nil, err
	}
	addrs := []*module.AddressDTO{}
	for _, addr := range response {
		addrs = append(addrs, addr.ToDTO())
	}

	return &module.RepeatedAddressDTO{Results: addrs}, nil
}

func (s *server) Update(ctx context.Context, in *module.UpdateAddressRequest) (*module.AddressDTO, error) {
	id, err := uuid.FromString(in.GetID())
	if err != nil || id == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad AddressID")
	}
	response, err := service.UpdateAddress(id, in)
	if err != nil {
		return nil, err
	}
	return response.ToDTO(), nil
}

func (s *server) Delete(ctx context.Context, in *module.DeleteAddressRequest) (*module.BoolResponse, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	isSuccess, err := service.DeleteAddress(ID)
	if err != nil {
		return nil, err
	}
	return &module.BoolResponse{IsSuccess: isSuccess}, nil
}

func runGRPCServer() {
	lis, err := net.Listen("tcp", ":8000")
	logger.CheckFatal(err)

	s := grpc.NewServer(
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				grpc_validator.StreamServerInterceptor(),
				service.StreamServerAuthInterceptor(),
				grpc_zap.StreamServerInterceptor(logger.GetLogger()),
			)),
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				grpc_validator.UnaryServerInterceptor(),
				service.UnaryServerAuthInterceptor(),
				grpc_zap.UnaryServerInterceptor(logger.GetLogger()),
			)),
	)
	module.RegisterAddressServiceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		logger.Error(fmt.Sprintf("failed to serve: %v", err))
	}

	serverChannel <- 1
}

func serveSwagger(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./Swagger/address.swagger.json")
}

func runRestServer() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := http.NewServeMux()
	gwmux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := module.RegisterAddressServiceHandlerFromEndpoint(ctx, gwmux, *echoEndpoint, opts)
	if logger.CheckError(err) {
		serverChannel <- 999
		return
	}
	mux.Handle("/", gwmux)
	mux.HandleFunc("/swagger.json", serveSwagger)
	fs := http.FileServer(http.Dir("Swagger/swagger-ui"))
	mux.Handle("/swagger-ui/", http.StripPrefix("/swagger-ui", fs))

	logger.CheckError(http.ListenAndServe(":80", mux))
	serverChannel <- 2
}

func main() {
	common.Bootstrap()
	common.DB.AutoMigrate(&entity.Address{}, &entity.CityState{})
	serverChannel = make(chan int)
	go runGRPCServer()
	go runRestServer()
	for {
		serverType := <-serverChannel
		if serverType == 1 {
			//GRPC
			logger.Info("Restarting GRPC endpoint")
			go runGRPCServer()
		} else if serverType == 2 {
			//Rest
			logger.Info("Restarting Rest endpoint")
			go runRestServer()
		} else {
			logger.Fatal("Failed to start some endPoint")
		}
	}
}
