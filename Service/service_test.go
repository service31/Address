package service

import (
	"testing"

	common "gitlab.com/service31/Common"
	entity "gitlab.com/service31/Data/Entity/Address"
)

func TestMain(m *testing.M) {
	SetupTesting()
	common.DB.AutoMigrate(&entity.Address{}, &entity.CityState{})
	defer DoneTesting()
	m.Run()
}

func TestCreateAddress(t *testing.T) {
	in, entityID := GetTestCreateAddressData()

	addr, err := CreateAddress(entityID, in)

	if err != nil {
		t.Error(err)
	} else if addr == nil {
		t.Error("Address is nil")
	}
}

func TestCreateAddressIsDefault(t *testing.T) {
	in, entityID := GetTestCreateAddressData()

	addr, err := CreateAddress(entityID, in)

	if err != nil {
		t.Error(err)
	} else if addr == nil {
		t.Error("Address is nil")
	} else if !addr.IsDefault {
		t.Error("Address is not default")
	}
}

func TestGetAddressByID(t *testing.T) {
	in, entityID := GetTestCreateAddressData()
	addr, err := CreateAddress(entityID, in)
	if err != nil {
		t.Error(err)
	} else if addr == nil {
		t.Error("Address is nil")
	}
	SetupAddress(addr)

	getAddr, err := GetAddressByID(addr.ID)

	if err != nil {
		t.Error(err)
	} else if getAddr == nil {
		t.Error("Get Address is nil")
	}
}

func TestGetDefaultAddressByEntity(t *testing.T) {
	in, entityID := GetTestCreateAddressData()
	addr, err := CreateAddress(entityID, in)
	if err != nil {
		t.Error(err)
	} else if addr == nil {
		t.Error("Address is nil")
	} else if !addr.IsDefault {
		t.Error("Address is not default")
	}
	SetupAddress(addr)

	getAddr, err := GetDefaultAddressByEntity(addr.EntityID)

	if err != nil {
		t.Error(err)
	} else if getAddr == nil {
		t.Error("Get Address is nil")
	}
}

func TestGetAddressesByEntityID(t *testing.T) {
	requests, entityID := GetTestTestGetAddressesByEntityIDData()
	for _, a := range requests {
		addr, err := CreateAddress(entityID, a)
		if err != nil {
			t.Error(err)
		} else if addr == nil {
			t.Error("Address is nil")
		}
		SetupAddress(addr)
	}

	addrs, err := GetAddressesByEntityID(entityID)

	if err != nil {
		t.Error(err)
	} else if len(addrs) != len(requests) {
		t.Errorf("%d created but %d returned", len(requests), len(addrs))
	}
}

func TestUpdateAddress(t *testing.T) {
	in, entityID := GetTestCreateAddressData()
	addr, err := CreateAddress(entityID, in)
	if err != nil {
		t.Error(err)
	} else if addr == nil {
		t.Error("Address is nil")
	} else if !addr.IsDefault {
		t.Error("Address is not default")
	}
	SetupAddress(addr)
	updateIn := GetTestUpdateAddressData(addr.ID)

	getAddr, err := UpdateAddress(addr.ID, updateIn)

	if err != nil {
		t.Error(err)
	} else if getAddr == nil {
		t.Error("Get Address is nil")
	} else if addr.Address1 == getAddr.Address1 {
		t.Error("Address1 is not updated")
	} else if addr.Firstname == getAddr.Firstname {
		t.Error("Firstname is not updated")
	} else if addr.Lastname == getAddr.Lastname {
		t.Error("Address1 is not updated")
	} else if addr.CityAndState.City == getAddr.CityAndState.City {
		t.Error("City is not updated")
	} else if addr.CityAndState.State == getAddr.CityAndState.State {
		t.Error("State is not updated")
	} else if addr.Zip == getAddr.Zip {
		t.Error("Zip is not updated")
	} else if addr.PhoneNumber == getAddr.PhoneNumber {
		t.Error("PhoneNumber is not updated")
	}
}

func TestDeleteAddress(t *testing.T) {
	in, entityID := GetTestCreateAddressData()
	addr, err := CreateAddress(entityID, in)
	if err != nil {
		t.Error(err)
	} else if addr == nil {
		t.Error("Address is nil")
	} else if !addr.IsDefault {
		t.Error("Address is not default")
	}
	SetupAddress(addr)

	response, err := DeleteAddress(addr.ID)

	if err != nil {
		t.Error(err)
	} else if !response {
		t.Error("Delete address responses false")
	}
}
