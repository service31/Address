package service

import (
	"context"
	"fmt"
	"strings"

	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redisClient "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/Address"
	module "gitlab.com/service31/Data/Module/Address"

	"github.com/go-redis/redis/v7"
	"github.com/gogo/status"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

//CreateAddress create new address for entity
func CreateAddress(entityID uuid.UUID, request *module.CreateAddressRequest) (*entity.Address, error) {
	entityID, err := uuid.FromString(request.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	addr := &entity.Address{
		EntityID:     entityID,
		Prefix:       request.GetPrefix(),
		Firstname:    request.GetFirstname(),
		Lastname:     request.GetLastname(),
		BusinessName: request.GetBusinessName(),
		IsBusiness:   request.GetIsBusiness(),
		Address1:     request.GetAddress1(),
		Address2:     request.GetAddress2(),
		Zip:          request.GetZip(),
		PhoneNumber:  request.GetPhoneNumber(),
		IsDefault:    false,
		IsActive:     true,
	}

	cityState, err := getCityStateID(request.GetCity(), request.GetState())
	if err != nil {
		return nil, err
	}

	addr.CityAndState = *cityState

	addrs, err := getAddressesByEntityID(entityID)
	if err != nil {
		return nil, err
	}

	if len(addrs) == 0 {
		addr.IsDefault = true
	}

	if err = common.DB.Save(addr).Scan(&addr).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to save address")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.AddressChange, addr.ID.String()))
	return addr, nil
}

//GetAddressByID get by ids addresse
func GetAddressByID(id uuid.UUID) (*entity.Address, error) {
	addr, err := getAddressByID(id)
	if err != nil {
		return nil, err
	}
	return addr, nil
}

//GetDefaultAddressByEntity get default address
func GetDefaultAddressByEntity(entityID uuid.UUID) (*entity.Address, error) {
	addr, err := getDefaultAddressByEntityID(entityID)
	if err != nil {
		return nil, err
	}
	return addr, nil
}

//GetAddressesByEntityID get addresses by entityID
func GetAddressesByEntityID(entityID uuid.UUID) ([]*entity.Address, error) {
	addrs, err := getAddressesByEntityID(entityID)
	if err != nil {
		return nil, err
	}
	if len(addrs) > 0 {
		return addrs, nil
	}
	return nil, status.Error(codes.NotFound, "")
}

//UpdateAddress update entity. If default, then mark other addresses default = false
func UpdateAddress(id uuid.UUID, request *module.UpdateAddressRequest) (*entity.Address, error) {
	addr, err := getAddressByID(id)
	if err != nil {
		return nil, err
	}

	cityState, err := getCityStateID(request.GetCity(), request.GetState())
	if err != nil {
		return nil, err
	}

	addr.Prefix = request.GetPrefix()
	addr.Firstname = request.GetFirstname()
	addr.Lastname = request.GetLastname()
	addr.BusinessName = request.GetBusinessName()
	addr.IsBusiness = request.GetIsBusiness()
	addr.Address1 = request.GetAddress1()
	addr.Address2 = request.GetAddress2()
	addr.Zip = request.GetZip()
	addr.PhoneNumber = request.GetPhoneNumber()
	addr.CityAndState = *cityState
	if err != nil {
		return nil, err
	}
	if !addr.IsDefault && request.GetIsDefault() {
		addrs, err := getAddressesByEntityID(addr.EntityID)
		if err != nil {
			return nil, err
		}
		for _, a := range addrs {
			a.IsDefault = false
		}

		err = common.DB.Transaction(func(tx *gorm.DB) error {
			if err = common.DB.Save(addrs).Error; logger.CheckError(err) {
				return status.Error(codes.Internal, "Failed to save addresses")
			}
			if err = common.DB.Save(addr).Scan(&addr).Error; logger.CheckError(err) {
				return status.Error(codes.Internal, "Failed to save updated address")
			}
			return nil
		})
		if err != nil {
			return nil, err
		}
	} else {
		if err = common.DB.Save(addr).Scan(&addr).Error; logger.CheckError(err) {
			return nil, status.Error(codes.Internal, "Failed to save updated address")
		}
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.AddressChange, addr.ID.String()))
	return addr, nil
}

//DeleteAddress by id
func DeleteAddress(id uuid.UUID) (bool, error) {
	addr, err := getAddressByID(id)
	if err != nil {
		return false, err
	}
	addr.IsActive = false
	if err = common.DB.Save(addr).Error; logger.CheckError(err) {
		return false, status.Error(codes.Internal, "Failed to delete address")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.AddressChange, addr.ID.String()))
	return true, nil
}

//UnaryServerAuthInterceptor address unary interceptor
func UnaryServerAuthInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		newCtx := ctx
		var err error
		newCtx, err = common.Authenticate(ctx)
		if err != nil {
			return nil, err
		}
		return handler(newCtx, req)
	}
}

//StreamServerAuthInterceptor address stream interceptor
func StreamServerAuthInterceptor() grpc.StreamServerInterceptor {
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		newCtx := stream.Context()
		var err error
		newCtx, err = common.Authenticate(stream.Context())
		if err != nil {
			return err
		}
		wrapped := grpc_middleware.WrapServerStream(stream)
		wrapped.WrappedContext = newCtx
		return handler(srv, wrapped)
	}
}

func getCityStateID(city string, state string) (*entity.CityState, error) {
	city = strings.TrimSpace(city)
	state = strings.TrimSpace(state)
	if city == "" || state == "" {
		return nil, status.Error(codes.InvalidArgument, "City and State cannot be empty")
	}

	cityState := &entity.CityState{}
	mapKey := redisClient.RedisMapKeys.GetCityStateMapKey(state, city)
	mapVal, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.CityStateHashMap, mapKey)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get city state")
	}

	err = redisClient.GetRedisValue(mapVal, cityState)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get city state")
	}

	return cityState, nil
}

func getAddressByID(id uuid.UUID) (*entity.Address, error) {
	addr := &entity.Address{}
	key := redisClient.RedisMapKeys.GetAddressKey(id.String())
	err := redisClient.GetRedisValue(key, &addr)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get default address")
	}
	return addr, nil
}

func getAddressesByIDs(ids []uuid.UUID) ([]*entity.Address, error) {
	addrs := []*entity.Address{}
	var keys []string
	for _, id := range ids {
		keys = append(keys, redisClient.RedisMapKeys.GetAddressKey(id.String()))
	}
	vals, err := redisClient.GetRedisValues(keys)
	if err == redis.Nil {
		return addrs, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get addresses")
	}
	for _, v := range vals {
		if v != nil {
			addr := &entity.Address{}
			err = addr.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				addrs = append(addrs, addr)
			}
		}
	}
	return addrs, nil
}

func getAddressesByEntityID(entityID uuid.UUID) ([]*entity.Address, error) {
	addrs := []*entity.Address{}

	mapVals, err := redisClient.GetRedisHashScanValues(redisClient.RedisMapKeys.AddressHashMap, redisClient.RedisMapKeys.GetAddressesByEntityMapKey(entityID.String()))
	if err == redis.Nil {
		return addrs, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get addresses")
	}

	if len(mapVals) < 1 {
		return addrs, nil
	}

	vals, err := redisClient.GetRedisValues(mapVals)
	if err == redis.Nil {
		return addrs, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get addresses")
	}
	for _, v := range vals {
		if v != nil {
			addr := &entity.Address{}
			err = addr.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				addrs = append(addrs, addr)
			}
		}
	}
	return addrs, nil
}

func getDefaultAddressByEntityID(entityID uuid.UUID) (*entity.Address, error) {
	addr := &entity.Address{}
	mapKey := redisClient.RedisMapKeys.GetDefaultAddressMapKey(entityID.String())
	key, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.AddressHashMap, mapKey)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get default address")
	}

	err = redisClient.GetRedisValue(key, &addr)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get default address")
	}
	return addr, nil
}
