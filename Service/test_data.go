package service

import (
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redisClient "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/Address"
	module "gitlab.com/service31/Data/Module/Address"
)

//SetupTesting Init test
func SetupTesting() {
	common.BootstrapTesting(false)
}

//DoneTesting close all connections
func DoneTesting() {
	defer common.DB.Close()
	defer redisClient.RedisClient.Close()
}

//GetTestCreateAddressData test data
func GetTestCreateAddressData() (*module.CreateAddressRequest, uuid.UUID) {
	entityID := uuid.NewV4()
	city := common.GetRandomString(5)
	state := common.GetRandomString(5)
	setupCityState(city, state)
	return &module.CreateAddressRequest{
		EntityID:    entityID.String(),
		Firstname:   common.GetRandomString(5),
		Lastname:    common.GetRandomString(5),
		IsBusiness:  false,
		Address1:    common.GetRandomString(25),
		City:        city,
		State:       state,
		Zip:         common.GetRandomNumberString(5),
		PhoneNumber: "+1" + common.GetRandomNumberString(10),
	}, entityID
}

//GetTestTestGetAddressesByEntityIDData test data
func GetTestTestGetAddressesByEntityIDData() ([]*module.CreateAddressRequest, uuid.UUID) {
	var requests []*module.CreateAddressRequest
	entityID := uuid.NewV4()
	city := common.GetRandomString(5)
	state := common.GetRandomString(5)
	setupCityState(city, state)
	for i := 0; i < 5; i++ {
		requests = append(requests,
			&module.CreateAddressRequest{
				EntityID:    entityID.String(),
				Firstname:   common.GetRandomString(5),
				Lastname:    common.GetRandomString(5),
				IsBusiness:  false,
				Address1:    common.GetRandomString(25),
				City:        city,
				State:       state,
				Zip:         common.GetRandomNumberString(5),
				PhoneNumber: "+1" + common.GetRandomNumberString(10),
			})
	}
	return requests, entityID
}

//GetTestUpdateAddressData test data
func GetTestUpdateAddressData(ID uuid.UUID) *module.UpdateAddressRequest {
	city := common.GetRandomString(5)
	state := common.GetRandomString(5)
	setupCityState(city, state)
	return &module.UpdateAddressRequest{
		ID:          ID.String(),
		Firstname:   common.GetRandomString(5),
		Lastname:    common.GetRandomString(5),
		IsBusiness:  false,
		Address1:    common.GetRandomString(25),
		City:        city,
		State:       state,
		Zip:         common.GetRandomNumberString(5),
		PhoneNumber: "+1" + common.GetRandomNumberString(10),
	}
}

//SetupAddress setup address is redis
func SetupAddress(address *entity.Address) {
	bytes, err := address.MarshalBinary()
	logger.CheckFatal(err)
	addressMapKey := redisClient.RedisMapKeys.GetAddressMapKey(address.EntityID.String(), address.ID.String())
	key := redisClient.RedisMapKeys.GetAddressKey(address.ID.String())
	redisClient.SetRedisValue(key, bytes)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.AddressHashMap, addressMapKey, key)
	if address.IsDefault {
		defaultMapKey := redisClient.RedisMapKeys.GetDefaultAddressMapKey(address.EntityID.String())
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.AddressHashMap, defaultMapKey, key)
	}
}

func setupCityState(city, state string) {
	cs := &entity.CityState{
		City:  city,
		State: state,
	}
	err := common.DB.Save(cs).Scan(&cs).Error
	logger.CheckFatal(err)
	bytes, err := cs.MarshalBinary()
	logger.CheckFatal(err)
	mapKey := redisClient.RedisMapKeys.GetCityStateMapKey(state, city)
	key := redisClient.RedisMapKeys.GetCityStateKey(cs.ID.String())
	err = redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.CityStateHashMap, mapKey, key)
	logger.CheckFatal(err)
	redisClient.SetRedisValue(key, bytes)
	logger.CheckFatal(err)
}
